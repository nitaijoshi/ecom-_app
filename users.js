var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
    username: {
        type: String,
    },
    password:{
        type:String
    },
    name:{
        type:String
    },
    phone:{
        type:Number
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema);