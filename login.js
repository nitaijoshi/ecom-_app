const express = require('express');
const mongoose = require('mongoose');
const app = express();
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const JWT = require('jsonwebtoken');
const UserModel = require('./users');

mongoose.connect('mongodb://localhost:27017/ecom', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log('Connection Established Successfully');
}).catch(err => {
    console.log(err)
    console.log('Error While Establishing Connection');
});



app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", true);
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS");
    next();
});
const VerifyAuthentication = (req, res, next) => {
    let token = req.headers.authorization.split(' ');
    token = token[1];
    JWT.verify(token, 'this is salt', function (err, decodedInfo) {
        if (err) {
            res.status(422).json({
                response: false,
                error: err.message
            });
        } else {
            req.body.decodedInfo = decodedInfo;
            next();
        }
    })
};
app.get("/", VerifyAuthentication, (req, res, next) => {
    UserModel.find({ _id: req.body.decodedInfo.id })
        .then((response) => {
            res.status(200).json({
                response: true,
                working_token: true
            });
        })
        .catch(err => {
            res.status(422).json({
                response: false,
                error: err
            });
        })
});

app.post("/signup", (req, res, next) => {
    bcrypt.hash(req.body.password, 10, function (error, pswd) {
        let user = new UserModel({
            username: req.body.username,
            password: pswd,
            name: req.body.name,
            phone: req.body.phone
        });
        user.save()
            .then((usr) => {
                if (usr) {
                    JWT.sign(
                        {
                            username: usr.username,
                            id: usr._id
                        }
                        , 'this is salt'
                        , function (err, token) {
                            if (err) {
                                res.status(422).json({
                                    err,
                                    response: false
                                });
                            } else {
                                res.status(200).json({
                                    response: true,
                                    token,
                                    msg: "Thanks for registering yourself with us. We will get back to you soon."
                                });
                            }
                        })
                }

            })
            .catch((err) => {
                res.status(422).json({
                    error: err,
                    response: false
                });
            });
    })

});

app.post("/login", (req, res, next) => {
    UserModel.findOne({ username: req.body.username })
        .then((user) => {
            if (user) {
                bcrypt.compare(req.body.password, user.password, function (err, response) {
                    if (response == true) {
                        JWT.sign(
                            {
                                username: user.username,
                                id: user._id
                            }
                            , 'this is salt'
                            , function (err, token) {
                                if (err) {
                                    res.status(422).json({
                                        err,
                                        response: false
                                    });
                                } else {
                                    res.status(200).json({
                                        response: true,
                                        token,
                                        msg: "Logged in successfully"
                                    });
                                }
                            })

                    } else {
                        res.status(422).json({
                            response: false,
                            msg: "Wrong Username/Password"
                        });
                    }
                });
            } else {
                res.status(422).json({
                    response: false,
                    msg: "Wrong Username/Password"
                });
            }
        });
});

app.listen(3000);